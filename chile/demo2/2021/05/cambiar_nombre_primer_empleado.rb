Admin::ConsoleAction.call(client_email: 'gnavarrete@buk.cl', what: 'Cambiar nombre primer empleado') do
  persona = Person.first
  persona.first_name = "Bruce"
  persona.last_name = "Willis"
  persona.save!
  puts "El nombre completo es: #{persona.first_name} #{persona.last_name}"
end
