Admin::ConsoleAction.call(client_email: 'gnavarrete@buk.cl', what: 'Cambiar nombre solicitado') do
  persona = Person.last
  persona.first_name = "Pedro"
  persona.last_name = "Pascal"
  persona.save!
  puts "El nombre completo es: #{persona.first_name} #{persona.last_name}"
end
