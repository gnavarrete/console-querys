# BUK Rapa Nui 🗿

Repositorio para alojar y automatizar el despliegue de querys en ambiente de producción.

## Instalación

- Clonar repositorio: git clone `https://gitlab.com/gnavarrete/console-querys`

## Subiendo una query

1. Crear una rama desde `master`.
2. Crear una query utilizando el servicio de `Admin::ConsoleAction`.
3. Guardar la query un archivo `.rb` utilizando `snake_case`.
3. Guardar el archivo según la siguiente estructura de carpeta según corresponda:  
```bash
cluster
└── tenant
    └── YYYY
        └── MM
            └── mi_query.rb
```
4. Pushear los cambios.

## Consideraciones

* Funciona para 1 query por MR.
* Funciona para 1 tenant por query.
